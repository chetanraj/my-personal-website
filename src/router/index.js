import Vue from 'vue'
import Router from 'vue-router'
import Website from '@/components/Website'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Website',
      component: Website
    }
  ]
})
